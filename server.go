// Simple notmuch json server
//
// WARNING: it have big security issues, it don't check the parameters 
//          before executing them on ty system

package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
)

func search(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Path[len("/search/"):]
	out, err := exec.Command("notmuch", "search", "--format=json", "--limit=50", query).Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, string(out))
}

func show(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Path[len("/show/"):]
	out, err := exec.Command("notmuch", "show", "--format=json", query).Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, string(out))
}

func tags(w http.ResponseWriter, r *http.Request) {
	out, err := exec.Command("notmuch", "search", "--format=json", "--output=tags", "*").Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, string(out))
}

func main() {
	http.HandleFunc("/search/", search)
	http.HandleFunc("/show/", show)
	http.HandleFunc("/tags/", tags)
	h := http.FileServer(http.Dir("."))
	http.Handle("/", http.StripPrefix("/", h))
	panic(http.ListenAndServe(":8080", nil))
}
