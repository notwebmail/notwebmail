$(function() {
	new SearchView({el: '#search'});
	$('#query').typeahead({source: searchAhead()});
});


/*
 * Collections
 */
var Thread = Backbone.Model.extend({});

var ThreadList = Backbone.Collection.extend({
	model: Thread,

	initialize: function(args) {
		this.url = args.url;
	}
});

var Message = Backbone.Model.extend({});

var MessageList = Backbone.Collection.extend({
	model: Message,

	initialize: function(args) {
		this.url = args.url;
	},

	parse: function(response) {
		var messages = [];
		var depth = 0;
		var parseBody = function(body) {
			var bodyStr = "";
			_.each(body, function(elem) {
				if (elem["content-type"] == "text/plain") {
					bodyStr += elem["content"];
				} else if (Array.isArray(elem["content"])) {
					bodyStr +=  parseBody(elem["content"]);
				}
			});
			return bodyStr;
		};
		var parser = function(list) {
			_.each(list, function(element) {
				if (Array.isArray(element)) {
					depth += 1;
					parser(element);
				} else {
					var msg = element["headers"];
					msg["tags"] = element["tags"];
					msg["body"] = parseBody(element["body"]);
					msg["depth"] = depth/2;
					msg["fold"] = !element["match"];
					messages.push(msg);
				}
			});
			depth -= 1;
		};
		parser(response);
		return messages;
	},
});


/*
 * Views
 */
var SearchView = Backbone.View.extend({
	events: {
		'submit': 'search'
	},

	search: function(event) {
		event.preventDefault();
		var query = this.$('#query').val();

		var threads = new ThreadList({url: '/search/' + query});
		new ThreadListView({el: '#threads', collection: threads, query: query});
		threads.fetch({update: true});
	}
});

var ThreadListView = Backbone.View.extend({
	initialize: function(args) {
		this.query = args["query"];
		this.$el.empty();
		$('#messages').empty(); //FIXME: should not be done on the MessageListView?
		this.collection.on("add", this.appendThread, this);
	},

	appendThread: function(model){
		var thread = new ThreadView({model: model, query: this.query});
		this.$el.append(thread.render().el);
	}
});

var ThreadView = Backbone.View.extend({
	events: {
		'click': 'openMessages'
	},

	template: Handlebars.compile($("#thread-template").html()),

	initialize: function(args) {
		this.query = args["query"];
	},

	render: function() {
		var html = this.template(this.model.toJSON());
		this.$el.html(html);
		return this;
	},

	openMessages: function(){
		$("p.select").removeClass("select");
		this.$("p").addClass("select");

		var threadId = this.model.get("thread");
		var messages = new MessageList({url: '/show/thread:' + threadId + " and " + this.query});
		new MessageListView({el: '#messages', collection: messages});
		messages.fetch({update: true});
	}
});

var MessageListView = Backbone.View.extend({
	initialize: function() {
		this.$el.empty();
		this.collection.on("add", this.appendMessage, this);
	},

	appendMessage: function(model){
		var message = new MessageView({model: model});
		this.$el.append(message.render().el);
	}
});

var MessageView = Backbone.View.extend({
	events: {
		'dblclick': 'toggleFold'
	},

	template: Handlebars.compile($("#message-template").html()),

	templateFold: Handlebars.compile($("#message-template-fold").html()),

	render: function() {
		var html;
		if (this.model.get("fold")) {
			html = this.templateFold(this.model.toJSON());
		} else {
			html = this.template(this.model.toJSON());
		}
		this.$el.html(html);
		return this;
	},

	toggleFold: function(){
		this.model.set("fold", !this.model.get("fold"));
		this.render();
	}
});


/*
 * Type ahead handler for the search box
 */
function searchAhead() {
	var tags = [];
	$.getJSON( "/tags/", function(resp) {
		tags = resp;
	});

	var lastWord = function(str) {
		return str.substring(str.lastIndexOf(" ")+1);
	};

	return function(query, process) {
		var word = lastWord(query);
		if (word.substring(0, 4) == "tag:") {
			var tag = word.substring(4);
			var append = query.substring(0, query.length-tag.length);
			var found = _.filter(tags, function(t) {
				return t.substring(0, tag.length) == tag;
			});
			return _.map(found, function(t) {
				return append + t;
			});
		}
	};
}
